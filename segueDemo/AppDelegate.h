//
//  AppDelegate.h
//  segueDemo
//
//  Created by Click Labs133 on 11/5/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

