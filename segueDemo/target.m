//
//  target.m
//  segueDemo
//
//  Created by Click Labs133 on 11/5/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "target.h"

@interface target ()
@property (strong, nonatomic) IBOutlet UIView *viewForDestination;
@property (strong, nonatomic) IBOutlet UILabel *name;

@end

@implementation target
@synthesize demoDest;
@synthesize name;
@synthesize arrayTarget;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    name.text=[arrayTarget objectAtIndex:1];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
